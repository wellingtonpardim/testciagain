export const Config = Symbol.for("Config");
export const DbConnection = Symbol.for("DbConnection");
export const ConnectionProvider = Symbol.for("ConnectionProvider");
export const Logger = Symbol.for("Logger");
export const NotificationService = Symbol.for("NotificationService");
export const HealthCheckUseCase = Symbol.for("HealthCheckUseCase");
