import { Container } from "inversify";
import { IDbConnection } from "Shared/Application/Contracts/Infrastructure/IDbConnection";
import { ILogger } from "Shared/Application/Contracts/Infrastructure/ILogger";
import { INotificationService } from "Shared/Application/Contracts/Infrastructure/Services/INotificationService";
import { IHealthCheckUseCase } from "Shared/Application/Contracts/UseCases/IHealthCheckUseCase";
import { HealthCheckUseCase } from "Shared/Application/UseCases/HealthCheckUseCase";
import { Config } from "Shared/Infrastructure/Config";
import { DbConnection } from "Shared/Infrastructure/Data/DbConnection";
import { DbFactory } from "Shared/Infrastructure/Data/DbFactory";
import { PinoLogger } from "Shared/Infrastructure/PinoLogger";
import { SNSNotificationService } from "Shared/Infrastructure/Services/Notification/SNSNotificationService";
import * as Types from "./Types";

class ServiceProvider {
  public static async GetContainer(correlationId: string): Promise<Container> {
    const container = new Container();

    const config = new Config();

    const connection = await DbFactory.Create();

    const dbConnection = new DbConnection(connection);

    container.bind<IDbConnection>(Types.DbConnection).toConstantValue(dbConnection);

    const logger = new PinoLogger(correlationId, config);

    container.bind<Config>(Types.Config).toConstantValue(config);

    container.bind<ILogger>(Types.Logger).toConstantValue(logger);

    container.bind<INotificationService>(Types.NotificationService).to(SNSNotificationService);

    container.bind<IHealthCheckUseCase>(Types.HealthCheckUseCase).to(HealthCheckUseCase);

    return container;
  }
}

export { ServiceProvider };
