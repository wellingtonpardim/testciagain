enum StatusCode {
  OK = 200,
  CREATED = 201,
  NO_CONTENT = 204,
  ERROR = 500,
  NOT_FOUND = 404,
  BAD_REQUEST = 400,
  UNAUTHORIZED = 401,
  FORBIDDEN = 403,
}

export { StatusCode };
