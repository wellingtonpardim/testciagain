class MigrationsList {
  public static get(): string[] {
    // tslint:disable-next-line: prefer-immediate-return
    const migrations: string[] = [`${__dirname}/Files/*{.js,.ts}`];

    return migrations;
  }
}

export { MigrationsList };
