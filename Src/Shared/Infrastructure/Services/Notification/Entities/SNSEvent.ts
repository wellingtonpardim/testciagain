import { MessageAttributeMap } from "aws-sdk/clients/sns";
import { DomainEvent } from "Shared/Domain/DomainEvent";

class SnsEvent {
  public Message: string;
  public MessageAttributes: MessageAttributeMap;
  public TopicArn: string;

  public constructor(topicArn: string, event: DomainEvent) {
    this.TopicArn = topicArn;
    this.Message = JSON.stringify(event);

    this.MessageAttributes = {
      eventType: {
        DataType: "String",
        StringValue: event.eventType,
      },
    };
  }
}

export { SnsEvent };
