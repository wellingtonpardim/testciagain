export interface ILogger {
  correlationId: string;
  debug(message: string, params?: object): void;
  error(message: string, ex?: Error, params?: object): void;
  info(message: string, params?: object): void;
  warn(message: string, params?: object): void;
}
