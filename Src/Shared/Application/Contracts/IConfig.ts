import { LoggerOptions } from "typeorm/logger/LoggerOptions";

interface IConfig {
  AppName: string;
  DatabaseHost: string;
  DatabaseInMemory: boolean;
  DatabaseLogging: LoggerOptions;
  DatabaseName: string;
  DatabasePassword: string;
  DatabasePort: number;
  DatabaseUser: string;
  EnableSwagger: boolean;
  LogLevel: string;
  SnsApiVersion: string;
  SnsAwsRegion: string;
  SnsTopicArn: string;
}

export { IConfig };
