import { ApplicationResult } from "../../Entities/ApplicationResult";
import { BaseRequest } from "../../Entities/BaseRequest";

interface IUseCase {
  execute(request?: BaseRequest): Promise<ApplicationResult> | ApplicationResult;
}

export { IUseCase };
