import { ApplicationResult } from "Shared/Application/Entities/ApplicationResult";
import { IUseCase } from "./IUseCase";

interface IHealthCheckUseCase extends IUseCase {
  execute(): Promise<ApplicationResult>;
}

export { IHealthCheckUseCase };
