class Notification {
  public readonly message: string;

  public readonly property: string;

  public constructor(message: string, property: string) {
    this.message = message.replace("{property}", property);
    this.property = property;
  }
}

export { Notification };
