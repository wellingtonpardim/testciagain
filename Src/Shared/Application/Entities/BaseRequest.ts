import { validateSync, ValidationError } from "class-validator";
import { Notification } from "./Notification";

abstract class BaseRequest {
  public ip: string;
  public notifications: Notification[];

  public constructor() {
    this.ip = "";
    this.notifications = [];
  }

  public get isValid(): boolean {
    const errors = validateSync(this);

    this.notifications = errors.map(
      (x: ValidationError): Notification => {
        const keys = Object.keys(x.constraints);
        const key = keys.slice(-1)[0];

        const message = x.constraints[key];

        return new Notification(message, x.property);
      }
    );

    return this.notifications.length === 0;
  }
}

export { BaseRequest };
