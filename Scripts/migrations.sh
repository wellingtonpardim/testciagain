#!/usr/bin/env sh

export DATABASE_HOST=localhost
export DATABASE_PORT=5432
export DATABASE_NAME=lapag_ledger_db
export DATABASE_USER=postgres
export DATABASE_PASSWORD=postgres
export DATABASE_LOGGING=query
export NODE_PATH=Src

export RUN_CONSUMER_MODULE=true

if [ "$1" = "up" ]; then
  npm run migrate:up
else
  if [ "$1" = "undo" ]; then
    npm run migrate:undo
  fi
fi
