#!/usr/bin/env sh

export START_EXPRESS=1
export APP_NAME=lapag-ledger
export LOG_LEVEL=error
export SHOW_ERROR=true
export NODE_PATH=Src:Test

export RUN_CONSUMER_MODULE=true

# If not run in CI context
if [ -z "$RUN_CI" ]; then
  export DATABASE_HOST=localhost
  export DATABASE_PORT=5432
  export DATABASE_NAME=lapag_ledger_db
  export DATABASE_USER=postgres
  export DATABASE_PASSWORD=postgres
  export DATABASE_LOGGING=all
fi

mocha --config .mocharc.yaml Test/**/Integration/**/*.spec.ts $1
